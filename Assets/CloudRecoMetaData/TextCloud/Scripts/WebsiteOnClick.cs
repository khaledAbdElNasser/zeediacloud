﻿using UnityEngine;

public class WebsiteOnClick : MonoBehaviour {

	public string website;
	
	void OnMouseDown() {
		Application.OpenURL(website);
	}
}
