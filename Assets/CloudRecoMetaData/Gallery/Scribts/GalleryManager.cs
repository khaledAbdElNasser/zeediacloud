﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GalleryManager : MonoBehaviour {

	//MATRIX
	public bool centerMatrixOnScreen = true; //Check this to move the matrix to the center of the screen (any value in MatrixPosition will be added to this = offset from center)
	public Vector3 matrixPosition = Vector3.zero; //This is the center of the matrix, use this to position the control - everything will rotate around this point!
	private Vector3 prevMatrixPosition;
	public float matrixAngle = 0.0f; //Use this to rotate the GUI //formerly known as globalAngle
	private float previousAngle; //used to check if the Angle changed, so the Quaternion doens't have to be calculated every frame
	private Quaternion quat = Quaternion.identity;
	private Matrix4x4 matrix = Matrix4x4.identity;
	public bool expandInputAreaToFullWidth = false; //Use the full width (left and right of the image) for swiping?
	bool MatrixIsSet = false;


	public RectTransform galleryContainer;
	public GameObject galleryItemPrefab;
	protected ArrayList galleryItems = new ArrayList ();
	public float spacing = 100.0f;
	bool isManualChangingPos = false;
	bool isAutomaticChangingPos = false; 

	public GameObject LoadingSpinner;
	public bool loading = true;
	
	protected CachedWWW mywww = new CachedWWW();
	public string[] CloudImgUrls;
	public Texture2D[] CloudImgaesTextures;
	protected int imagesLoadingCounter = 0;

	//Swipe Vars
	int currentImage = 0;


	//Zoom & Pan Variables
	public bool isFullScreen = false;
	public GameObject prevBtn;
	public GameObject nextBtn;
	public GameObject backBtn;
	public GameObject shareBtn;
	public GameObject fullScreenBtn;
    public GameObject cloudTarget;

    //public GameObject cloudTarget; 
    float scale = 1.0f;
	bool firstTimeZoom = true;
	float firstTimeDistance;
	public float maxZoomScale = 2.0f;
	public float minZoomScale = 1.0f;
	float panX = 0.0f;
	float panY = 0.0f;

	//Scrolling
	bool isTouched = false;
	public ScrollRect containerScroller;
	float downPosX = 0.0f;
	float upPosX = 0.0f;


	
	// Use this for initialization
	void Start () {
		if(centerMatrixOnScreen) { 
			matrixPosition.x += Mathf.Round(Screen.width * 0.5f);
			matrixPosition.y += Mathf.Round(Screen.height * 0.5f);
		}
      
        
    }

	void AddItem(Texture2D image) {
		GameObject newItem = GameObject.Instantiate (galleryItemPrefab);
		galleryItems.Add (newItem);
		newItem.GetComponent<Image> ().sprite = Sprite.Create (image, new Rect (0.0f, 0.0f, image.width, image.height), new Vector2 (0.5f, 0.5f));
		newItem.transform.SetParent(galleryContainer,false);
		RectTransform newItemRectTransform =  newItem.GetComponent<RectTransform> ();
		newItemRectTransform.localScale = new Vector3(1.0f,1.0f,1.0f);
		newItemRectTransform.localRotation = Quaternion.FromToRotation(Vector3.zero,Vector3.zero);
		newItemRectTransform.localPosition = Vector3.zero;
		Vector3 itemnewPosition = newItemRectTransform.localPosition;
		
		itemnewPosition.x = (newItemRectTransform.rect.width+spacing) *  (galleryItems.Count-1);
		newItemRectTransform.localPosition = itemnewPosition;
		galleryContainer.sizeDelta = new Vector2 ((newItemRectTransform.rect.width * galleryItems.Count) + spacing * (galleryItems.Count-1) ,galleryContainer.sizeDelta.y);
		Debug.Log ("Delta Size is "+galleryContainer.sizeDelta );
	}
	
	public void DestroyGallery(){
		foreach (GameObject item in galleryItems) {
			Destroy(item);
		}
		galleryItems.Clear ();
		
		CloudImgaesTextures = null;

		Debug.Log ("Destroy Gallery Manager");
	}

	public void ToggleFullScreen(){
		if (loading)
			return;
		scale = 1.0f;
		panX = 0.0f;
		panY = 0.0f;
		
		isFullScreen = !isFullScreen;
	//	prevBtn.SetActive (!isFullScreen);
	//	nextBtn.SetActive (!isFullScreen);
		backBtn.SetActive (!isFullScreen);
		shareBtn.SetActive (!isFullScreen);
		fullScreenBtn.SetActive (!isFullScreen);
		containerScroller.gameObject.SetActive (!isFullScreen);
        cloudTarget.GetComponent<ZeediaCloudRecoTrackableEventHandler>().fullScreen = true;
        Debug.Log ("Full Screen Pressed , www is ");
	}

	public void Prev (){
		if(CloudImgaesTextures == null || isManualChangingPos) {
			return;
		}
		if (currentImage > 0) {
			currentImage = currentImage - 1;
			StartCoroutine (MoveToCurrent ());
		}
		
	}
	
	
	public void Next (){
		if( CloudImgaesTextures == null || isManualChangingPos) {
			return;
		}
		if (currentImage + 1 < CloudImgaesTextures.Length) {
			currentImage = currentImage + 1;
			StartCoroutine (MoveToCurrent());
		}
		
	}
	
	
	void OnMouseUp(){
		if (!isAutomaticChangingPos && !isManualChangingPos) {
			if(upPosX > downPosX+100.0f)
				Prev();
			else if(upPosX < downPosX-100.0f)
				Next();
			else
				StartCoroutine(MoveToCurrent());
			downPosX = 0.0f;
			upPosX = 0.0f;
		}
	}

	IEnumerator MoveToCurrent(){
		isManualChangingPos = true;
			Vector3 newposition;
			newposition = galleryContainer.anchoredPosition3D;
			Debug.Log("Current Image is "+((int) Mathf.Abs(Mathf.Ceil(galleryContainer.localPosition.x / (galleryItemPrefab.GetComponent<RectTransform> ().sizeDelta.x+spacing)))));
			Debug.Log("Current Image should be " + currentImage);
			float rightPostion = -(currentImage * (galleryItemPrefab.GetComponent<RectTransform>().sizeDelta.x+spacing));
		if (galleryContainer.anchoredPosition3D.x < rightPostion) {
			while(galleryContainer.anchoredPosition3D.x < rightPostion){
				newposition.x = newposition.x + 100.0f;
				galleryContainer.anchoredPosition3D = newposition;
				Debug.Log("Current Container Position is "+ galleryContainer.localPosition.x);
				yield return false;
			}
		} else if(galleryContainer.anchoredPosition3D.x > rightPostion) {
			while(galleryContainer.anchoredPosition3D.x > rightPostion){
				newposition.x = newposition.x - 100.0f;
				galleryContainer.anchoredPosition3D = newposition;
				Debug.Log("Current Container Position is "+ galleryContainer.localPosition.x);
				yield return false;
			}
			
		}
		containerScroller.StopMovement ();
		newposition.x = rightPostion;
		galleryContainer.anchoredPosition3D = newposition;
		isManualChangingPos = false;
	}

	// Update is called once per frame
	void OnGUI () {
		

		if (Input.GetMouseButtonDown (0)) {
			isTouched = true;
			downPosX = Input.mousePosition.x;
		}

		if (Input.GetMouseButtonUp (0) && isTouched) {
			isTouched = false;
			upPosX = Input.mousePosition.x;
			if(!isFullScreen)
				OnMouseUp();
		}


		
		if(loading && LoadingSpinner) {
			
			return;
		}
		// GUI MATRIX
		if((matrixAngle != previousAngle || matrixPosition != prevMatrixPosition) && !MatrixIsSet ) { //only calculate new Quaternion if angle changed
			MatrixIsSet = true;
			quat.eulerAngles = new Vector3(0.0f, 0.0f, matrixAngle);
			previousAngle = matrixAngle;
			matrix = Matrix4x4.TRS(matrixPosition, quat, Vector3.one);	//If you're no longer tweaking
			prevMatrixPosition = matrixPosition;
		
		}
		GUI.matrix = matrix;

		
		if (!isFullScreen) {

		} else {
			if(CloudImgaesTextures != null && CloudImgaesTextures[currentImage] != null){
				float mainPosX = -Screen.width * scale * 0.5f +panX;
				float mainPosY = -Screen.height * scale * 0.5f +panY;
				Debug.Log("Current Image is " + currentImage);
				GUI.DrawTexture (new Rect (mainPosX , mainPosY, Screen.width * scale, Screen.height * scale), CloudImgaesTextures [currentImage]);
				Rect fullScreenBtnRect = new Rect (Screen.width*0.5f - 200 ,Screen.height * 0.5f-200 ,120,120);
				GUI.DrawTexture (fullScreenBtnRect, fullScreenBtn.GetComponent<Image>().mainTexture);
				if(Input.touches.Length >= 1){
					Vector2 screenTouch = Input.touches[0].position;
					screenTouch.y = Screen.height - screenTouch.y;
                    if (fullScreenBtnRect.Contains(screenTouch - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)))
                    {
                        
                            ToggleFullScreen();
                        gameObject.SetActive(false);
                        cloudTarget.GetComponent<ZeediaCloudRecoTrackableEventHandler>().fullScreen = false;
                        cloudTarget.GetComponent<ZeediaCloudRecoTrackableEventHandler>().ResetTracking();
                        
                       
                    }
                    
                }
			}
		}
		
		if(Input.touchCount == 1)
		{
			PanUpdate(Input.touches[0].deltaPosition);
		}
		else if(Input.touchCount == 2)
		{
			if(firstTimeZoom){
				firstTimeDistance = Vector3.Distance(Input.touches[0].position, Input.touches[1].position) ;
				firstTimeZoom = false;
			}
			ZoomUpdate(  Vector3.Distance(Input.touches[0].position, Input.touches[1].position) );

		}
		
		if (Input.touchCount < 2) {
			firstTimeZoom = true;
		}	
	}


	public void positionChanged () {
		isAutomaticChangingPos = true;
		if(!isManualChangingPos)
			currentImage =(int) Mathf.Abs(Mathf.Ceil(galleryContainer.localPosition.x / (galleryItemPrefab.GetComponent<RectTransform> ().sizeDelta.x+spacing)));
		isAutomaticChangingPos = false;
	}

	public void LoadGalleryImages (string[] urls) {
		Debug.Log("GalleryDisplay - Load(1) urls = " + urls.ToString());
		if(LoadingSpinner) {
			LoadingSpinner.SetActive(true);
		}
		loading = true;
		CloudImgUrls = urls;
		//		if(CloudImg.Length == 0) {
		//			return false;
		//		}
		CloudImgaesTextures = new Texture2D[CloudImgUrls.Length];
		imagesLoadingCounter = 0;
		for(int i=0; i < CloudImgUrls.Length; i++) {
			Debug.Log("GalleryDisplay - Load(2) url "+imagesLoadingCounter+" = " + CloudImgUrls[imagesLoadingCounter]);
			
			StartCoroutine(mywww.CoroutineLoadFromCacheOrDownload(CloudImgUrls[i].Replace("https","http"),CachedWWW.CachingType.TEXTURE
                                                                 , OnProgress, OnComplete));
		}
		
	}

    void OnProgress(float progress)
    {
        if (LoadingSpinner)
        {
            LoadingSpinner.GetComponentInChildren<UnityEngine.UI.Text>().text = Mathf.Floor(((progress + imagesLoadingCounter) * 100) / CloudImgUrls.Length) + "%";
        }
    }

    void OnComplete(WWW www, string cachingpath){
		if(!www.isDone)
			Debug.Log("Download is not finished");
		if(www.error != null)
			Debug.Log("Downloading Error " + www.error);
		else{
			CloudImgaesTextures[imagesLoadingCounter]  = www.texture;
			Debug.Log("Downloading finihsed well 1");
			AddItem(CloudImgaesTextures[imagesLoadingCounter]);
		}
		imagesLoadingCounter++;
		if (imagesLoadingCounter == CloudImgUrls.Length) {
			loading = false;
			if(LoadingSpinner != null)
				LoadingSpinner.SetActive(false);
		}
		if (www != null) {
			www.Dispose();
			www = null;
		}
	}
	
	void ZoomUpdate(float distance){
		Debug.Log ("distance is " + distance);
		if(distance > firstTimeDistance)
			scale += 0.08f;
		else if(distance < firstTimeDistance)
			scale -= 0.08f;
		
		if (scale > maxZoomScale)
			scale = maxZoomScale;
		if (scale < minZoomScale)
			scale = minZoomScale;
		
		if (-Screen.width * scale * 0.5f+panX  > -Screen.width * 0.5f)
			panX = (Screen.width * scale * 0.5f)+(-Screen.width * 0.5f);
		if (-Screen.height * scale * 0.5f + panY > -Screen.height * 0.5f)
			panY = (Screen.height * scale * 0.5f) + (-Screen.height * 0.5f);
		
		if (-Screen.width * scale * 0.5f+panX + (Screen.width * scale) < Screen.width * 0.5f)
			panX = (Screen.width * scale * 0.5f)-(Screen.width * scale)+(Screen.width * 0.5f);
		if (-Screen.height * scale * 0.5f+panY + (Screen.height * scale) < Screen.height * 0.5f)
			panY = (Screen.height * scale * 0.5f)-(Screen.height * scale)+(Screen.height * 0.5f);
		
		firstTimeDistance = distance;
		
	}
	void PanUpdate(Vector2 position) {
		panX += position.x;
		panY -= position.y;
		
		if (-Screen.width * scale * 0.5f+panX  > -Screen.width * 0.5f)
			panX = (Screen.width * scale * 0.5f)+(-Screen.width * 0.5f);
		if (-Screen.height * scale * 0.5f + panY > -Screen.height * 0.5f)
			panY = (Screen.height * scale * 0.5f) + (-Screen.height * 0.5f);
		
		if (-Screen.width * scale * 0.5f+panX + (Screen.width * scale) < Screen.width * 0.5f)
			panX = (Screen.width * scale * 0.5f)-(Screen.width * scale)+(Screen.width * 0.5f);
		if (-Screen.height * scale * 0.5f+panY + (Screen.height * scale) < Screen.height * 0.5f)
			panY = (Screen.height * scale * 0.5f)-(Screen.height * scale)+(Screen.height * 0.5f);
	}
}
