using UnityEngine;
using System.Collections;

public class GoogleMap : MonoBehaviour
{
    public enum MapType
    {
        RoadMap,
        Satellite,
        Terrain,
        Hybrid
    }

    public bool autoLocateCenter = true;
    public GoogleMapLocation centerLocation;
    public int zoom = 13;
    public MapType mapType;
    public int size = 512;
    public bool doubleResolution = false;
    public GoogleMapMarker[] markers;
    public GoogleMapPath[] paths;
    int currentZoom;
    public int defaultZoom = 15;
    bool refreshing = false;
    string mapUrl = "";
    CachedWWW req = new CachedWWW();
    public GameObject LoadingSpinner;
   
   
    public void Load(GoogleMapLocation map, string mapurl)
    {
        
        zoom = defaultZoom;
        currentZoom = zoom;
      
        markers[0].size = GoogleMapMarker.GoogleMapMarkerSize.Mid;


        markers[0].color = GoogleMapColor.blue;
      
        markers[0].label = map.address;

        markers[0].locations[0].latitude = map.latitude;
        markers[0].locations[0].longitude = map.longitude;

        centerLocation.latitude = map.latitude;
        centerLocation.longitude = map.longitude;
        mapUrl = mapurl;
      
        Refresh();
    }

    public void DestroyMap()
    {
        GetComponent<UnityEngine.UI.Image>().sprite = null;
        zoom = defaultZoom;
        currentZoom = zoom;
        centerLocation.latitude = 0.0f;
        centerLocation.longitude = 0.0f;
        markers[0].label = "";
        mapUrl = "";
    }

    public void ZoomIn()
    {
        if (!refreshing && currentZoom < 19)
            currentZoom++;
        else
            return;
        if (currentZoom != zoom)
        {
            zoom = currentZoom;
            Refresh();
        }
    }

    public void BrowseInGoogleMaps()
    {
        if (mapUrl != "")
            Application.OpenURL(mapUrl);
    }

    public void ZoomOut()
    {
        if (!refreshing && currentZoom > 0)
            currentZoom--;
        else
            return;
        if (currentZoom != zoom)
        {
            zoom = currentZoom;
            Refresh();
        }
    }


    public void Refresh()
    {
        if (autoLocateCenter && (markers.Length == 0 && paths.Length == 0))
        {
            Debug.LogError("Auto Center will only work if paths or markers are used.");
        }
        _Refresh();
    }


    void _Refresh()
    {
        refreshing = true;
        LoadingSpinner.SetActive(true);
        var url = "http://maps.googleapis.com/maps/api/staticmap";
        var qs = "";
        if (!autoLocateCenter)
        {
            if (centerLocation.address != "")
                qs += "center=" + WWW.UnEscapeURL(centerLocation.address);
            else
            {
                qs += "center=" + WWW.UnEscapeURL(string.Format("{0},{1}", centerLocation.latitude, centerLocation.longitude));
            }

            qs += "&zoom=" + zoom.ToString();
        }
        qs += "&size=" + WWW.UnEscapeURL(string.Format("{0}x{0}", size));
        qs += "&scale=" + (doubleResolution ? "2" : "1");
        qs += "&maptype=" + mapType.ToString().ToLower();
        var usingSensor = false;
#if UNITY_IPHONE
		usingSensor = Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Running;
#endif
        qs += "&sensor=" + (usingSensor ? "true" : "false");
#if UNITY_IPHONE
		foreach (var i in markers) {
			qs += "&markers=" + string.Format ("size:{0}%7Ccolor:{1}%7Clabel:{2}", i.size.ToString ().ToLower (), i.color, WWW.EscapeURL(i.label));
			foreach (var loc in i.locations) {
				if (loc.address != ""){
					qs += "%7C" + WWW.UnEscapeURL (loc.address);
				} else {
					qs += "%7C" + WWW.UnEscapeURL (string.Format ("{0},{1}", loc.latitude, loc.longitude));
				}
			}
		}
		
		foreach (var i in paths) {
			qs += "&path=" + string.Format ("weight:{0}|color:{1}", i.weight, i.color);
			if(i.fill) qs += "|fillcolor:" + i.fillColor;
			foreach (var loc in i.locations) {
				if (loc.address != "")
					qs += "%7C" + WWW.UnEscapeURL (loc.address);
				else
					qs += "%7C" + WWW.UnEscapeURL (string.Format ("{0},{1}", loc.latitude, loc.longitude));
			}
		}
#else
        foreach (var i in markers)
        {
            qs += "&markers=" + string.Format("size:{0}|color:{1}|label:{2}", i.size.ToString().ToLower(), i.color, WWW.EscapeURL(i.label));
            foreach (var loc in i.locations)
            {
                if (loc.address != "")
                {
                    qs += "|" + WWW.UnEscapeURL(loc.address);
                }
                else
                {
                    qs += "|" + WWW.UnEscapeURL(string.Format("{0},{1}", loc.latitude, loc.longitude));
                }
            }
        }

        foreach (var i in paths)
        {
            qs += "&path=" + string.Format("weight:{0}|color:{1}", i.weight, i.color);
            if (i.fill) qs += "|fillcolor:" + i.fillColor;
            foreach (var loc in i.locations)
            {
                if (loc.address != "")
                    qs += "|" + WWW.UnEscapeURL(loc.address);
                else
                    qs += "|" + WWW.UnEscapeURL(string.Format("{0},{1}", loc.latitude, loc.longitude));
            }
        }

#endif


        Debug.Log("URL of Map is " + url + "?" + qs);
        
        StartCoroutine(req.CoroutineLoadFromCacheOrDownload(url + "?" + qs, CachedWWW.CachingType.TEXTURE, OnProgress, OnComplete));
        
    }

    public void OnProgress(float progress)
    {
        if (LoadingSpinner)
        {
            LoadingSpinner.GetComponentInChildren<UnityEngine.UI.Text>().text = Mathf.Floor(progress * 100) + "%";
        }
    }

    public void OnComplete(WWW www, string cachingpath)
    {
        GetComponent<UnityEngine.UI.Image>().sprite = Sprite.Create(www.texture, new Rect(0.0f, 0.0f, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f)); ;
        www.Dispose();
        www = null;
        LoadingSpinner.SetActive(false);
        refreshing = false;
    }
}

public enum GoogleMapColor
{
    black,
    brown,
    green,
    purple,
    yellow,
    blue,
    gray,
    orange,
    red,
    white
}

[System.Serializable]
public class GoogleMapLocation
{
    public string address;
    public float latitude;
    public float longitude;
}

[System.Serializable]
public class GoogleMapMarker
{
    public enum GoogleMapMarkerSize
    {
        Tiny,
        Small,
        Mid
    }
    public GoogleMapMarkerSize size;
    public GoogleMapColor color;
    public string label;
    public GoogleMapLocation[] locations;

}

[System.Serializable]
public class GoogleMapPath
{
    public int weight = 5;
    public GoogleMapColor color;
    public bool fill = false;
    public GoogleMapColor fillColor;
    public GoogleMapLocation[] locations;
}