﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GalleryVoteManager : GalleryManager {

	public votingManager voteManager;
	VoteData voteData;

	public void LoadVoteGalleryImages (VoteData data) {
		voteData = data;
		//Debug.Log("GalleryDisplay - Load(1) urls = " + urls.ToString());
		if(LoadingSpinner) {
			LoadingSpinner.SetActive(true);
		}
		loading = true;

		string[] urls = new string[data.votesAnswers.Length];
		for(int i = 0 ; i < urls.Length ; i++){
			urls[i] = data.votesAnswers[i].value;
		}

		CloudImgUrls = urls;
		//		if(CloudImg.Length == 0) {
		//			return false;
		//		}
		CloudImgaesTextures = new Texture2D[CloudImgUrls.Length];
		imagesLoadingCounter = 0;
		for(int i=0; i < CloudImgUrls.Length; i++) {
			Debug.Log("GalleryDisplay - Load(2) url "+imagesLoadingCounter+" = " + CloudImgUrls[imagesLoadingCounter]);
			/*www = new WWW(CloudImg[counter].Replace("https","http"));
			yield return www;
			if(!www.isDone)
				Debug.Log("Download is not finished");
			if(www.error != null)
				Debug.Log("Downloading Error " + www.error);
			img[counter] = www.texture;*/
			StartCoroutine(mywww.CoroutineLoadFromCacheOrDownload(CloudImgUrls[i].Replace("https","http"),CachedWWW.CachingType.TEXTURE
			                                                      ,OnProgress,OnComplete));
		}
		/*loading = false;
		Debug.Log("GalleryDisplay - Load(3)");
		if(LoadingSpinner) {
			LoadingSpinner.SetActive(false);
		}*/
	}
	void OnProgress(float progress){
		if(LoadingSpinner){
			LoadingSpinner.GetComponentInChildren<UnityEngine.UI.Text>().text = Mathf.Floor(((progress + imagesLoadingCounter) * 100)  / CloudImgUrls.Length)+"%";
		}
	}
	
	void OnComplete(WWW www, string cachingpath){
		if(www == null || !www.isDone)
			Debug.Log("Download is not finished");
		if(www == null || www.error != null)
			Debug.Log("Downloading Error " + ((www == null)?"www is null":www.error) );
		else{
			CloudImgaesTextures[imagesLoadingCounter]  = www.texture;
			Debug.Log("Downloading finihsed well 1");
			AddItem(CloudImgaesTextures[imagesLoadingCounter],voteData.votesAnswers[imagesLoadingCounter].id,voteData.votesAnswers[imagesLoadingCounter].value,voteData.votesAnswers[imagesLoadingCounter].name);
		}
		imagesLoadingCounter++;
		if (imagesLoadingCounter == CloudImgUrls.Length) {
			loading = false;
			if(LoadingSpinner != null)
				LoadingSpinner.SetActive(false);
		}
		if (www != null) {
			www.Dispose();
			www = null;
		}
	}
	void AddItem(Texture2D image,string id, string url, string name) {
		GameObject newItem = GameObject.Instantiate (galleryItemPrefab);
		galleryItems.Add (newItem);
		newItem.GetComponent<Image> ().sprite = Sprite.Create (image, new Rect (0.0f, 0.0f, image.width, image.height), new Vector2 (0.5f, 0.5f));
		newItem.transform.SetParent(galleryContainer,false);
		RectTransform newItemRectTransform =  newItem.GetComponent<RectTransform> ();
		newItemRectTransform.localScale = new Vector3(1.0f,1.0f,1.0f);
		newItemRectTransform.localRotation = Quaternion.FromToRotation(Vector3.zero,Vector3.zero);
		newItemRectTransform.localPosition = Vector3.zero;
		Vector3 itemnewPosition = newItemRectTransform.localPosition;
		
		itemnewPosition.x = (newItemRectTransform.rect.width+spacing) *  (galleryItems.Count-1);
		newItemRectTransform.localPosition = itemnewPosition;
		galleryContainer.sizeDelta = new Vector2 ((newItemRectTransform.rect.width * galleryItems.Count) + spacing * (galleryItems.Count-1) ,galleryContainer.sizeDelta.y);
		Debug.Log ("Delta Size is "+galleryContainer.sizeDelta );
		newItem.GetComponentInChildren<Toggle> ().onValueChanged.AddListener (voteManager.OnUserVoted);
		newItem.GetComponent<GalleryVoteItemData> ().id = id;
		newItem.GetComponent<GalleryVoteItemData> ().url = url;
		newItem.GetComponentInChildren<Text> ().text = name;
		voteManager.GalleryVotesUI.Add (newItem.GetComponent<GalleryVoteItemData> ());
	}

}
