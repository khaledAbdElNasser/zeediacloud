﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class votingManager : chartManager {

	VoteData votedata;

	public Text voteTitleUI;
	public Toggle[] votesUI;
	public Button submitBtn;
	public List<GalleryVoteItemData>  GalleryVotesUI = new List<GalleryVoteItemData>();
	public GalleryVoteManager GalleryVote;
	ChartData voteResult;
	public GameObject textVoteBackBtn;
	public GameObject TextVoteShareBtn;


	void Start() {

	}

	/*void OnEnable(){
		//Test Data;
		Debug.Log("hiiiii");
		votedata = new VoteData ();
		votedata.voteTitleText = "Vote Title : please choose one on the following option";
		votedata.votesText = new string[3];
		votedata.votesText[0] = "vote 1 option : may be the best option what do you think ?";
		votedata.votesText[1] = "vote 2 option : may be no that good option what do you think ?";
		votedata.votesText[2] = "vote 3 option : we advice you not to choose it.";
		ShowVote (votedata);
	}*/

	public void ShowVote(VoteData data) {
		votedata = data;
		StartCoroutine(GetVoteData());
	}

	IEnumerator GetVoteData(){
		textVoteBackBtn.SetActive (false);
		TextVoteShareBtn.SetActive (false);
		GalleryVote.gameObject.SetActive (false);
		string getVoteParametersFormat = votedata.voteServerUrl + "?title={0}";

		WWW voteDataRequest = new WWW (string.Format(getVoteParametersFormat,WWW.EscapeURL(votedata.voteTitle)));
		yield return voteDataRequest;
		string response = voteDataRequest.text;
		Debug.Log("vote data response is "+ response);
		voteDataRequest.Dispose ();
		voteDataRequest = null;

		var voteDataJson = SimpleJSON.JSON.Parse (response);
		votedata.voteID = voteDataJson ["id"];
		votedata.voteText = voteDataJson ["text"];
		votedata.voteType = voteDataJson ["type"];
		Debug.Log ("vote text is " + votedata.voteText);
		SimpleJSON.JSONNode answersJson = voteDataJson ["answers"];
		votedata.votesAnswers = new VoteAnswer[answersJson.AsArray.Count];
		for (int i = 0; i < votedata.votesAnswers.Length; i++) {
			votedata.votesAnswers[i] = new VoteAnswer();
			votedata.votesAnswers[i].id = answersJson[i]["id"];
			votedata.votesAnswers[i].value = answersJson[i]["value"];
			Debug.Log("Answer "+ votedata.votesAnswers[i].id + " value is "+ votedata.votesAnswers[i].value);
		}

		Debug.Log ("Vote Type is " + votedata.voteType);
		if (votedata.voteType == "text") { //text vote
			voteTitleUI.text = voteDataJson ["text"];
			submitBtn.gameObject.SetActive (false);
			for (int i = 0; i < votesUI.Length; i++) {
				if (i < votedata.votesAnswers.Length) {
					Debug.Log ("setting item " + i);
					votesUI [i].isOn = false;
					votesUI [i].gameObject.SetActive (true);
					votesUI [i].GetComponentInChildren<Text> ().text = votedata.votesAnswers[i].value;
					votesUI [i].onValueChanged.AddListener (OnUserVoted);
				} else
					votesUI [i].gameObject.SetActive (false);
			}
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
			foreach (Renderer component in rendererComponents) {
				component.enabled = true;
			}
			textVoteBackBtn.SetActive (true);
			TextVoteShareBtn.SetActive (true);
		} else { // image vote
			voteTitleUI.text = voteDataJson ["text"];
			submitBtn.gameObject.SetActive (false);
			GalleryVote.gameObject.SetActive (true);
			GalleryVote.LoadVoteGalleryImages(votedata);
		}
	}
	


	public void OnUserVoted (bool status){
		bool readyToSubmit = false;
		if (votedata.voteType == "text") {
			for (int i = 0; i < votesUI.Length; i++) {
				if (votesUI [i].isOn)
					readyToSubmit = true;
			}
		} else {
			for (int i = 0; i < GalleryVotesUI.Count; i++) {
				if (GalleryVotesUI [i].GetComponentInChildren<Toggle>().isOn )
					readyToSubmit = true;
			}
		}
		submitBtn.gameObject.SetActive (readyToSubmit);
	}

	public void SubmitVote(){
		voteTitleUI.text = "Submitting... please wait!";
		submitBtn.gameObject.SetActive (false);
		string answers = "";
		if (votedata.voteType == "text") {
			for (int i = 0; i < votesUI.Length; i++) {
				if(votesUI[i].isOn){
					answers = answers + votedata.votesAnswers[i].id+",";
				}
				votesUI[i].gameObject.SetActive(false);
			}
		}else{
			for (int i = 0; i < GalleryVotesUI.Count; i++) {
				if(GalleryVotesUI[i].GetComponentInChildren<Toggle>().isOn){
					answers = answers + GalleryVotesUI[i].id+",";
				}
			}
			GalleryVote.gameObject.SetActive(false);
		}
		answers = answers.Remove (answers.Length - 1);
		Debug.Log ("answers ids are " + answers);
		// Connect to server here to submit vote data and retrive vote results.
		StartCoroutine (SubmitVoteData (answers));

	}

	IEnumerator SubmitVoteData (string answers){


		WWWForm submitResultParameters = new WWWForm ();
		submitResultParameters.AddField ("question_id", votedata.voteID);
		submitResultParameters.AddField ("device_id", SystemInfo.deviceUniqueIdentifier);
		submitResultParameters.AddField ("answers", answers);
		string SubmitResultUrlFormat = votedata.voteServerUrl + "?question_id={0}&device_id={1}&answers={2}";
		//WWW voteSubmitRequest = new WWW (string.Format (SubmitResultUrlFormat, votedata.voteID, SystemInfo.deviceUniqueIdentifier, answers));
		WWW voteSubmitRequest = new WWW (votedata.voteServerUrl, submitResultParameters);
		yield return voteSubmitRequest;
		string resultsJSon = voteSubmitRequest.text;
		showResults (resultsJSon);
	}

	void showResults(string resultsJson) {
		Debug.Log ("Results are " + resultsJson);
		var voteResultsDataJson = SimpleJSON.JSON.Parse (resultsJson);
		voteTitleUI.text = voteResultsDataJson["message"];
		int allvotesCount = voteResultsDataJson ["all"].AsInt;
		int answersCount = voteResultsDataJson ["answers"].AsArray.Count;

		string[] votesLabelsTest = new string[answersCount];
		float[] votesResultsValuesTest = new float[answersCount];

		////Test Data ///
		
		for (int i = 0; i < answersCount; i++) {
			if(votedata.voteType == "text")
				votesLabelsTest [i] = voteResultsDataJson ["answers"][i]["answer"];
			else{
				votesLabelsTest [i] = voteResultsDataJson ["answers"][i]["name"];
				if(votesLabelsTest [i] == null || votesLabelsTest [i] == ""){
					votesLabelsTest [i] = "image "+ i.ToString();
				}
			}
			votesResultsValuesTest [i] =  Mathf.Floor ((float)voteResultsDataJson ["answers"][i]["count"].AsInt / (float)allvotesCount * 100.0f);
		}

		
		//End Testing Data
		
		voteResult = new ChartData ();
		voteResult.labels = votesLabelsTest;
		voteResult.values = votesResultsValuesTest;
		voteResult.maxValue = Mathf.Max(voteResult.values);
		for (int i = 0; i < voteResult.labels.Length; i++) {
			voteResult.labels[i] = voteResult.labels[i] + ", " + voteResult.values[i].ToString(); 
		}
		LoadChart (voteResult);
	}
	string GetAnswerText(string id){
		for (int i = 0; i < votedata.votesAnswers.Length; i++) {
			if(votedata.votesAnswers[i].id ==  id){
				return votedata.votesAnswers[i].value;
			}
		}
		return "";
	}

	public void DestroyVote() {
		if (GalleryVote != null) {
			GalleryVote.DestroyGallery();
			GalleryVotesUI.Clear();
			GalleryVote = null;
		}
		if (votedata != null) {
			votedata.voteText = null;
			votedata.voteTitle = null;
			votedata.voteType = null;
			if(votedata.votesAnswers != null ){
				for(int i = 0 ; i < votedata.votesAnswers.Length ; i ++){
					votedata.votesAnswers[i] = null;
				}
				votedata.votesAnswers = null;
			}
			votedata = null;
		}
		for (int i = 0; i < votesUI.Length; i++) {
			votesUI[i].isOn = false;
			votesUI[i].gameObject.SetActive(false);
		}
		submitBtn.gameObject.SetActive (false);
		if (voteResult != null) {
			voteResult.labels = null;
			voteResult.values = null;
			voteResult = null;
		}
		DestroyChart ();
	}


}

public class VoteData{
	public string voteID;
	public string voteTitle;
	public string voteText;
	public string voteType;
	public VoteAnswer[] votesAnswers;
	public string voteServerUrl = "http://destinationksa.zeedia.net/vote.php";
}
public class VoteAnswer{
	public string id;
	public string value;
	public string name;
}