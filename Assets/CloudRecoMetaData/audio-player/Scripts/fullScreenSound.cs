﻿using UnityEngine;
using UnityEngine.UI;

public class fullScreenSound : MonoBehaviour {

    public Scrollbar soundBar;
    public Button PlayPauseBtn;

    public Sprite playBg;
    public Sprite pauseBg;

    public AudioSource soundSource;

    public GameObject bollFullScreen;
    // Use this for initialization
    void Start()
    {
        soundSource = GetComponent<AudioSource>();
        soundSource.enabled = true;
        soundSource.playOnAwake = true;
        soundSource.Play();
        
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {

        if (!soundSource.clip || soundSource.clip.loadState != AudioDataLoadState.Loaded)
        {
            return;
        }

        if (soundBar && soundSource && soundSource.isPlaying)
        {
            soundBar.value = soundSource.time / soundSource.clip.length;
        }

        if (soundSource.isPlaying)
        {
            PlayPauseBtn.image.sprite = pauseBg;
        }
        else
        {
            PlayPauseBtn.image.sprite = playBg;
        }

    }
    public void back()
    {
        bollFullScreen.GetComponent<ZeediaCloudRecoTrackableEventHandler>().fullScreen = false;
    }
    public void Change()
    {
        soundSource.time = soundBar.value * soundSource.clip.length;
    }

    public void PlayPause()
    {
        if (soundSource.isPlaying)
        {
            soundSource.Pause();
        }
        else
        {
            soundSource.Play();
        }
    }
}
