﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SwitchBackground : MonoBehaviour {

	public Sprite[] Backgrounds;

	private Sprite Background;
	private int Index = 0;

	// Use this for initialization
	void Start () {
		if(Backgrounds.Length > 0) {
			Background = Backgrounds[Index];
		}
		InvokeRepeating("ChangeBackground", 0.1f, 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Image>().sprite = Background;
	}

	void ChangeBackground() {
		AudioSource sound = GetComponentInParent<AudioSource>();

		if(sound && sound.isPlaying)
			Background = Backgrounds[++Index%Backgrounds.Length];
	}
}
