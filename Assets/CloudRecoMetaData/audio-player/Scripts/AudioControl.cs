﻿using UnityEngine;
using UnityEngine.UI;

public class AudioControl : MonoBehaviour
{

    public Scrollbar soundBar;
    public Button PlayPauseBtn;

    public Sprite playBg;
    public Sprite pauseBg;



    public GameObject bollFullScreen;
    private AudioSource soundSource;


    public GameObject LoadingSpinner;
    public GameObject fullScreenSoundobj;
    WWW www;
    CachedWWW mywww = new CachedWWW();

    // Use this for initialization
    void Start()
    {
        soundSource = GetComponent<AudioSource>();

    }


    public void Load(string url)
    {
        if (LoadingSpinner)
        {
            LoadingSpinner.SetActive(true);
        }

        soundBar.value = 0;
        PlayPauseBtn.image.sprite = playBg;

        if (!soundSource)
        {
            soundSource = GetComponent<AudioSource>();
        }
        soundSource.playOnAwake = true;

        StartCoroutine(mywww.CoroutineLoadFromCacheOrDownload(url, CachedWWW.CachingType.AUDIO, OnProgress, DownloadComplete));

    }
    void OnProgress(float progress)
    {
        if (LoadingSpinner != null)
        {
            LoadingSpinner.GetComponentInChildren<UnityEngine.UI.Text>().text = Mathf.Floor(progress * 100) + "%";
        }
    }
    void DownloadComplete(WWW www, string cachepath)
    {
        soundSource.enabled = true;
        soundSource.clip = www.GetAudioClip(false, false);
        if (LoadingSpinner)
        {
            LoadingSpinner.SetActive(false);
        }
        Invoke("PlayPause", 1.0f);
        Debug.Log("AudioControl - 33");
        www.Dispose();
        www = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (!soundSource.clip || soundSource.clip.loadState != AudioDataLoadState.Loaded)
        {
            return;
        }

        if (soundBar && soundSource && soundSource.isPlaying)
        {
            soundBar.value = soundSource.time / soundSource.clip.length;
        }


        if (soundSource.isPlaying)
        {
            PlayPauseBtn.image.sprite = pauseBg;
        }
        else
        {
            PlayPauseBtn.image.sprite = playBg;
        }

    }
    public void FullScreen()
    {
        bollFullScreen.GetComponent<ZeediaCloudRecoTrackableEventHandler>().fullScreen = true;
        fullScreenSoundobj.GetComponent<AudioSource>().clip = soundSource.clip;
        fullScreenSoundobj.SetActive(true);
        gameObject.SetActive(false);
    }
    public void Change()
    {
        soundSource.time = soundBar.value * soundSource.clip.length;
    }

    public void PlayPause()
    {
        if (soundSource.isPlaying)
        {
            soundSource.Pause();
        }
        else
        {
            soundSource.Play();
        }
    }
}
