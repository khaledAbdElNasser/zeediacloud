﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Dissolve : MonoBehaviour {

	public GameObject Loader;

    Renderer rend;
	float markerRatio;

	private bool Touched=false;
	private bool loading = true;

	CachedWWW mywww = new CachedWWW();
	//////////////////// test Scratch	
	Color[] colors;
	public Color32 tt;
	Vector2 pixelUV;
	/// <summary>
	/// ///////////
	/// </summary>
	int brushSize = 150;

	public Texture2D tex ;

	Texture2D paintTex;
	public Texture2D brush;
	RaycastHit hit;
	Color currentColor;
	Color[] colorsToEdit;
	List<int> indexes;
	//float startT;
	Vector2 lastScratchedPoint;
	public TextMesh generatedNumberID;
	public bool isNumberGenerator = false;
	public GameObject textBackground;


	void Start() {
		//StartCoroutine(Load ("http://images6.fanpop.com/image/photos/32600000/Beautiful-Nature-Wallpaper-random-32603809-1024-768.jpg"));ulgluglu
		colors = brush.GetPixels ();
		indexes = new List<int> ();
		for (int i = 0; i < colors.Length; i++) {
			if(colors[i].a < 1.0f)
				indexes.Add(i);
		}
		
		lastScratchedPoint.x = -10000.0f;
		Debug.Log ("Indexies size is " + indexes.Count);
	}


	public void Load(string url,bool isNumberGeneratorFeature,float ratioOfMarker) {
		isNumberGenerator = isNumberGeneratorFeature;
		markerRatio = ratioOfMarker;
		loading = true;
		Loader.SetActive (true);
		transform.localScale = Vector3.one;
		
		StartCoroutine (mywww.CoroutineLoadFromCacheOrDownload (url, CachedWWW.CachingType.TEXTURE, OnProgress, OnComplete));
	}

    void OnProgress(float progress)
    {
        if (loading && Loader)
        {
            Loader.GetComponentInChildren<Text>().text = Mathf.Floor(progress * 100) + "%";
        }
    }
    void OnComplete(WWW www, string cachedpath){
		if (paintTex != null)
			paintTex = null;
		paintTex = new Texture2D (www.texture.width, www.texture.height);
		paintTex.LoadImage (www.texture.EncodeToPNG());
		
		if (colors == null)
			Debug.Log ("colors are null");
		else
			Debug.Log ("colors size is " + colors.Length);
		
		rend = GetComponent<Renderer> ();
		rend.material.mainTexture = paintTex;
		
		loading = false;
		Loader.SetActive (false);
		Resize ();
		www.Dispose ();
		www = null;
		if (isNumberGenerator) {
			StartCoroutine(GenerateNumber());
		} else {
			textBackground.SetActive(false);
		}
	}

	IEnumerator GenerateNumber(){
		textBackground.SetActive(true);
		WWWForm parameters = new WWWForm ();
		parameters.AddField ("device_id", SystemInfo.deviceUniqueIdentifier);
		WWW generateRequest = new WWW ("http://destinationksa.zeedia.net/scratch.php", parameters);
		yield return generateRequest;
		generatedNumberID.text = generateRequest.text;
	}

	void Resize() {
		Debug.Log ("ratio is " + markerRatio);
		if (markerRatio < 1.0f) {
			Vector3 cs = transform.localScale;
			cs.x = markerRatio * cs.y;
			transform.localScale = cs;
		} else if (markerRatio > 1.0) {
			Vector3 cs = transform.localScale;
			cs.y = (1.0f / markerRatio) * cs.x;
			transform.localScale = cs;
		} else {
			transform.localScale = Vector3.one;
		}

	} 
	 
	void Update() {
		
		if(!loading && Touched && finishedWriting) {
			Debug.Log("should scratch");
			Disoolve();
		}

		if (Input.touches.Length == 0 && !Input.GetMouseButton (0)) {
			lastScratchedPoint.x = -10000.0f;
		}

	}

	void OnMouseDown() {
		Touched = true;
	}
	void OnMouseUp(){
		Touched = false;
	}

	bool finishedWriting = true;
	void Disoolve()
	{
		finishedWriting = false;

#if UNITY_EDITOR
		if (!Input.GetMouseButton(0)){
			finishedWriting = true;
			return;
		}
		
		if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit)){
			finishedWriting = true;
			return;
		}
#else
		if (Input.touches.Length == 0){
			finishedWriting = true;
			return;
		}
		
		if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.touches[0].position), out hit)){
			finishedWriting = true;
			return;
		}
#endif

		pixelUV = hit.textureCoord;
		pixelUV.x *= paintTex.width;
		pixelUV.y *= paintTex.height;


		if (Vector2.Distance (pixelUV, lastScratchedPoint) > brushSize / 2 && lastScratchedPoint.x != -10000.0f) {
			Debug.Log ("Drawing Line Brush");
			StartCoroutine (DrawBrushLine (lastScratchedPoint, pixelUV));
		} else {
			paintWithBrush (pixelUV);
			finishedWriting = true;
			//textureChanged = true;
			paintTex.Apply ();
		}
		lastScratchedPoint = pixelUV;
	}

	IEnumerator DrawBrushLine(Vector2 start, Vector2 end){
		Vector2 currentPoint = start;
		while (Vector2.Distance(currentPoint,end) > brushSize/2) {
			currentPoint = Vector2.MoveTowards(currentPoint,end,brushSize/2);
			paintWithBrush(currentPoint);
			yield return true;
		}
		finishedWriting = true;
		//textureChanged = true;
		paintTex.Apply ();
	}

	void paintWithBrush(Vector2 point){
		int clampedX = Mathf.Clamp((int)point.x-(brushSize/2),0,paintTex.width);
		int clampedY = Mathf.Clamp((int)point.y-(brushSize/2),0,paintTex.height);
		int ClampedBlockWidth = Mathf.Clamp (brushSize, 0, paintTex.width - clampedX);
		int ClampedBlockHeight = Mathf.Clamp (brushSize, 0, paintTex.height - clampedY);
		Color[] colorsToEdit = paintTex.GetPixels(clampedX,clampedY, ClampedBlockWidth,ClampedBlockHeight);
		
		for(int i = 0; i < indexes.Count ; i++){
			if(indexes[i] < colorsToEdit.Length)
				colorsToEdit[indexes[i]].a = colorsToEdit[indexes[i]].a -( 1.0f - colors[indexes[i]].a);
		}
		paintTex.SetPixels(clampedX,clampedY, ClampedBlockWidth,ClampedBlockHeight, colorsToEdit);
	}

	public void DestroyScratch(){
		paintTex = null;
		rend.material.mainTexture = null;
		rend.material.SetTexture ("_SliceGuide", null);
		textBackground.SetActive(false);
		
	}
	
}