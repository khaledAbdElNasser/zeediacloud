﻿using UnityEngine;
using System.Collections;

public class chartManager : MonoBehaviour {

	public GameObject chartContainer;
	public string[] labels;
	public float[] values;
	public GameObject chartItemPrefab;
	public ArrayList items = new ArrayList(); //chartItemMAnager[]
	float spacing = 1.0f;
	string chartUrl;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadChart(ChartData data){
		labels = data.labels;
		values = data.values;
		chartUrl = data.chartUrl;
		items.Clear ();
		for (int i = 0; i < labels.Length; i++) {
			items.Add(GameObject.Instantiate(chartItemPrefab).GetComponent<chartItemManager>());
			((chartItemManager)items[i]).transform.SetParent( chartContainer.transform);
			((chartItemManager)items[i]).transform.localPosition = new Vector3( (40.0f + spacing) * i,0,0); 
			((chartItemManager)items[i]).setLabel(labels[i]);
			((chartItemManager)items[i]).setHeight(values[i]+100,data.maxValue);
			((chartItemManager)items[i]).setColor(Choose(Color.red,Color.green,Color.blue,Color.cyan,Color.magenta,Color.white,Color.yellow));
		}

	}

	public void OpenOriginalChart() {
		if (chartUrl != "")
			Application.OpenURL (chartUrl);
	}

	public void DestroyChart(){
		for (int i = 0; i < items.Count; i++) {
			Destroy(((chartItemManager)items[i]).gameObject);
		}
		items.Clear ();
		labels = null;
		values = null;
	}

	public T Choose<T>(T a, T b, params T[] p)
	{
		int random = Random.Range(0, p.Length + 2);
		if (random == 0) return a;
		if (random == 1) return b;
		return p[random - 2];
	}
}

public class ChartData {
	public string[] labels;
	public float[] values;
	public float maxValue;
	public string chartUrl;
}
