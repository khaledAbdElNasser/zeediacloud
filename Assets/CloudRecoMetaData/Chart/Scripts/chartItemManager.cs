﻿using UnityEngine;
using System.Collections;

public class chartItemManager : MonoBehaviour {

	public GameObject column;
	public TextMesh label;

	public void setHeight(float value,float maxValue){
		Vector3 newScaleCol = column.transform.localScale;
		Vector3 newPosLabel = label.transform.localPosition;
		newScaleCol.y = Mathf.Floor ((value / maxValue)*100);
		newPosLabel.y = newScaleCol.y / 2.0f;

		iTween.ScaleTo (column, newScaleCol , 2.0f);

	}

	public void setLabel(string text){
		label.text = text;
	}

	public void setColor(Color c){
		column.transform.GetChild (0).gameObject.GetComponent<Renderer> ().material.color = c;
	}

	public void setLabelColor (Color c){
		label.color = c;
	}


	void Update () {
		if (label.transform.localPosition.y != column.transform.localScale.y / 2) {
			Vector3 newPosLabel = label.transform.localPosition;
			newPosLabel.y = column.transform.localScale.y / 2;
			label.transform.localPosition = newPosLabel;
		}
	}

}
