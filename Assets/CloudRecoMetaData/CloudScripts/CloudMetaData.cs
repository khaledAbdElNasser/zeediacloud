﻿/*==============================================================================
Copyright (A) 2016 Hive-ad Connected Experiences, Inc.
All Rights Reserved.
==============================================================================*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloudMetaData : MonoBehaviour
{

    public string Type;
    public List<string> Data;
    public float markerRatio;
    public CalendarData calendarData;
    public List<CalendarData> CalendarDataList = new List<CalendarData>();
    public ChartData chartData;
    public VoteData voteData;

    public float VRatio;
    public GoogleMapLocation mapData;
    public bool doubleTapped = false;

    /*List<float> objScaleX = new List<float>();
    List<float> objScaleY = new List<float>();*/
    public float objScaleX;
    public float objScaleY;

    public List<GameObject> linksItems = new List<GameObject>();
    public GameObject instantiatText;
    public GameObject instantiatCalendar;
    List<float> positionsX = new List<float>();
    List<float> positionsY = new List<float>();

    public GameObject mapCanvas;
    public GameObject videoCanvas;
    public GameObject scratchObj;
    public GameObject TextCanv;
    public GameObject videoObject;
    public GameObject calendarCanv;
    public GameObject audioCanvas;
    public GameObject Gallery;
    public GameObject scratchCanvas;
    public GameObject chart;
    public GameObject vote;

    public GameObject cube;
    void Start()
    {
        //for (int i = 0; i < 4; i++) {
        // Instantiate the shattered target prefab in place of this target.

        //	GameObject textObj = Instantiate (instantiatText, new Vector3 (/*0.0f,LinkText.transform.position.y, (i*30.0f) */0.0f ,0.0f ,0.0f),  Quaternion.Euler(90, 0, 0)) as GameObject;

        //	if(i%2 ==1)
        //	{
        //		textObj.transform.localPosition = new Vector3 (0.0f, /*0.0135f*/instantiatText.transform.position.y, (i*40.0f));
        //	}
        //	else
        //	{
        //		textObj.transform.localPosition = new Vector3 (0.0f, /*0.0135f*/instantiatText.transform.position.y, -(i*30.0f) );
        //	}
        //	textObj.transform.localScale = new Vector3 (4.0f,40.0f,12.0f);

        //	linksItems.Add(textObj);

        //}

        //LinkText.SetActive (true);

        // InputController.DoubleTapped += HandleDoubleTap;
    }
    int k = 0;
    /*void OnGUI() {
		
		if (GUI.Button (new Rect (10, 70, 50, 30), "Click")) {
			k++;
			foreach (GameObject item in linksItems) {

				Destroy(item);
			}

			linksItems.Clear ();
			Debug.Log ("Clicked the button with text"+k);
		}
	}*/


    public void TargetSearch(string metaData)
    {

        var data = SimpleJSON.JSON.Parse(metaData);

        Type = data["Type"];

        markerRatio = data["ratio"].AsFloat;

        if (Type == "video")
        {
            VRatio = data["ratio"].AsFloat;
            /* if (VRatio < 1.0f){videoObject.transform.localScale = new Vector3(VRatio, 1.0f, 1.0f);}*/
        }
        if (Type == "map")
        {

            mapData.longitude = data["longitude"].AsFloat;
            mapData.latitude = data["latitude"].AsFloat;
            mapData.address = data["name"];
            VRatio = data["ratio"].AsFloat;
        }
        SimpleJSON.JSONNode urls = data["url"];
        Data.Clear();

        if (urls != null)
        {
            for (int i = 0; i < urls.AsArray.Count; i++)
            {
                Data.Add(urls[i]);
            }
        }
        SimpleJSON.JSONNode PositionsX = data["positionsx"];
        SimpleJSON.JSONNode PositionsY = data["positionsy"];
        positionsY.Clear();
        positionsX.Clear();
        for (int i = 0; i < PositionsX.Count; i++)
        {
            positionsX.Add(PositionsX[i].AsFloat);
        }
        for (int i = 0; i < PositionsY.Count; i++)
        {
            positionsY.Add(PositionsY[i].AsFloat);
        }
        /* SimpleJSON.JSONNode ScaleX = data["Scalex"];
         SimpleJSON.JSONNode ScaleY = data["Scaley"];
         objScaleX.Clear();
         objScaleY.Clear();
         for (int i = 0; i < ScaleX.Count; i++)
         {
             objScaleX.Add(ScaleX[i].AsFloat);
         }
         for (int i = 0; i < ScaleY.Count; i++)
         {
             objScaleY.Add(ScaleY[i].AsFloat);
         }*/
        objScaleX = data["Scalex"].AsFloat;
        objScaleY = data["Scaley"].AsFloat;

        if (Type == "calendar")
        {

            if (calendarData != null)
                calendarData = null;

            SimpleJSON.JSONNode eventtitleJson = data["eventtitle"];
            SimpleJSON.JSONNode eventdescriptionJson = data["eventdescription"];
            SimpleJSON.JSONNode eventlocationJson = data["eventlocation"];
            SimpleJSON.JSONNode starttimeJson = data["starttime"];
            SimpleJSON.JSONNode endtimeJson = data["endtime"];
            CalendarDataList.Clear();
            for (int i = 0; i < eventtitleJson.AsArray.Count; i++)
            {
                calendarData = new CalendarData();
                calendarData.eventTitle = eventtitleJson[i];
                calendarData.eventdescription = eventdescriptionJson[i];
                calendarData.eventLocation = eventlocationJson[i];
                calendarData.startTime = starttimeJson[i];
                calendarData.endTime = endtimeJson[i];
                CalendarDataList.Add(calendarData);
                calendarData = null;
            }
        }
        if (Type == "chart")
        {
            if (chartData != null)
                chartData = null;
            
            SimpleJSON.JSONNode jsonLabels = data["labels"];
            SimpleJSON.JSONNode jsonValues = data["values"];
            string[] labels = new string[jsonLabels.AsArray.Count];
            float[] values = new float[jsonValues.AsArray.Count];

            for (int i = 0; i </* labels.Length*/ jsonLabels.AsArray.Count; i++)
            {
                labels[i] = jsonLabels[i];
                values[i] = jsonValues[i].AsFloat;
                 }


             chartData = new ChartData();
                chartData.labels = labels; //jsonLabels[i];
                chartData.values = values; //jsonValues[i].AsFloat;
           
            chartData.maxValue = data["maxvalue"].AsFloat;
            chartData.chartUrl = urls[0];
        }

        if (Type == "vote")
        {
            if (voteData != null)
                voteData = null;


             voteData = new VoteData();
             voteData.voteTitle = data["title"];
             voteData.voteServerUrl = Data[0];
            
        }

    }

    public GameObject getCurrentCanvas()
    {
        switch (Type)
        {
            case "gallery":
                {
                   
                    Gallery.gameObject.SetActive(true);
                    GalleryManager gd = Gallery.GetComponent<GalleryManager>();
                    if (gd)
                    {
                        gd.LoadGalleryImages(Data.ToArray());
                    }
                }
                return Gallery;

            case "audio":
                audioCanvas.gameObject.SetActive(true);
                audioCanvas.GetComponent<AudioControl>().Load(Data[0]);

                return audioCanvas;

            case "text":

                TextCanv.SetActive(true);

                for (int i = 0; i < Data.Count; i++)
                {


                    GameObject textObj = Instantiate(instantiatText, new Vector3(positionsX[i], instantiatText.transform.position.y, positionsY[i]), Quaternion.Euler(0, -90, 0)) as GameObject;


                    textObj.transform.localScale = new Vector3(objScaleX, 0.1f, objScaleY);

                    textManager link = textObj.GetComponent<textManager>();
                    link.websiteC = Data[i];
                    linksItems.Add(textObj);
                }

                return TextCanv;
            case "calendar":

                calendarCanv.SetActive(true);
                for (int i = 0; i < Data.Count; i++)
                {
                    GameObject textObj = Instantiate(instantiatCalendar, new Vector3(positionsX[i], instantiatCalendar.transform.position.y, positionsY[i]), Quaternion.Euler(90, 180, 0)) as GameObject;

                    textObj.transform.localScale = new Vector3(objScaleX, 0.1f, objScaleY);

                    calendarManager gdC = textObj.GetComponent<calendarManager>();
                    if (gdC)
                    {
                        gdC.initCalender(CalendarDataList[i]/*, Data[i]*/);
                    }
                    linksItems.Add(textObj);
                }
                return calendarCanv;

            case "video":

                videoObject.gameObject.SetActive(true);
                videoCanvas.gameObject.SetActive(true);
                videoObject.GetComponent<MediaPlayerCtrl>().Load(Data[0]);

                videoObject.GetComponent<MediaPlayerCtrl>().OnReady = delegate {
                    //currentObject.GetComponent<MediaPlayerCtrl>().m_bAutoPlay = true;      //currentObject.GetComponent<MediaPlayerCtrl>().Play();
                    Invoke("autoplay", 1.0f);
                };
                videoObject.GetComponent<MediaPlayerCtrl>().m_bFullScreen = false;
                return videoCanvas;

            case "map":

                mapCanvas.gameObject.SetActive(true);
                GoogleMap gMapManager = mapCanvas.GetComponentInChildren<GoogleMap>();
                if (gMapManager)
                {
                    if (VRatio < 1.0f)
                    {
                        mapCanvas.transform.localScale = new Vector3(0.0005f, 0.0005f * VRatio, 0.0005f);
                    }
                    gMapManager.Load(mapData, Data[0]);
                }
                return mapCanvas;
            case "scratch":

                scratchObj.gameObject.SetActive(true);
                scratchCanvas.gameObject.SetActive(true);

                Dissolve disv = scratchObj.GetComponent<Dissolve>();

                disv.Load(Data[0], false, markerRatio);

                return scratchCanvas;

            case "scratchnumber":

                scratchObj.gameObject.SetActive(true);
                scratchCanvas.gameObject.SetActive(true);
                Dissolve disvnumber = scratchObj.GetComponent<Dissolve>();
                disvnumber.Load(Data[0], true, markerRatio);

                return scratchCanvas;

            case "chart":

                chart.SetActive(true);
                chartManager gChartManager = chart.GetComponent<chartManager>();
                if (gChartManager)
                {
                   
                    gChartManager.LoadChart(chartData);
                }
   

        return chart;
            case "vote":

                vote.SetActive(true);
        votingManager gVotetManager = vote.GetComponent<votingManager>();
        if (gVotetManager)
        {
            gVotetManager.ShowVote(voteData);
        }

        return vote;

            default:
                return null;
        }
    }

    public void DestroyCurrentObject()
    {

        switch (Type)
        {
            case "gallery":

                if (Gallery.GetComponent<GalleryManager>().isFullScreen)
                {
                    return;
                }
                else
                {
                    Gallery.GetComponent<GalleryManager>().DestroyGallery();
                    Gallery.gameObject.SetActive(false);
                }
                break;

            case "audio":

                if (audioCanvas.GetComponent<AudioSource>().clip)
                {
                    audioCanvas.GetComponent<AudioSource>().Stop();
                }
                audioCanvas.gameObject.SetActive(false);

                break;
            case "scratch":
                scratchObj.GetComponent<Dissolve>().DestroyScratch();
                scratchObj.GetComponent<Renderer>().enabled = false;
                scratchObj.gameObject.SetActive(false);
                scratchCanvas.gameObject.SetActive(false);
                break;
            case "scratchnumber":
                scratchObj.GetComponent<Dissolve>().DestroyScratch();
                scratchObj.GetComponent<Renderer>().enabled = false;
                scratchObj.gameObject.SetActive(false);
                scratchCanvas.gameObject.SetActive(false);
                break;
            case "text":

                TextCanv.gameObject.SetActive(false);
                foreach (GameObject item in linksItems)
                {
                    Destroy(item);
                }
                linksItems.Clear();
                break;
            case "calendar":

                calendarCanv.gameObject.SetActive(false);
                foreach (GameObject item in linksItems)
                {
                    Destroy(item);
                }
                linksItems.Clear();
                break;

            case "video":

                videoObject.GetComponent<MediaPlayerCtrl>().Pause();
                videoObject.GetComponent<MediaPlayerCtrl>().UnLoad();

                videoObject.SetActive(false);
                videoCanvas.SetActive(false);

                break;
            case "map":

                mapCanvas.GetComponentInChildren<GoogleMap>().DestroyMap();
                mapCanvas.SetActive(false);
                break;
            case "chart":

                chart.SetActive(false);
                if (chart != null)
                    chart.GetComponent<chartManager>().DestroyChart();

                
                break;
            case "vote":
                vote.SetActive(false);
                if (vote != null)
                    vote.GetComponent<votingManager>().DestroyVote();

               
                break;
            default:
                break;

        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}