﻿using System;
using UnityEngine;
using System.Collections;

public class SceneAssetBundleLoader : MonoBehaviour {
	WWW www;
	private string BundleURL;
	private int version;
	private string AssetName;
	private UnityEngine.Object LoadedBundleObject;
	public GameObject Loading;
	bool finisheddownload;
	private string CurrentAsset;
	public CloudRecoTrackableEventHandler CloudTargetObj;

	public void LoadBundle(string Url, string ScenePrefabName, int ver, bool enableCache = true){
		finisheddownload = false;
		BundleURL = Url;
		version = ver;
		AssetName = ScenePrefabName;
		
		if(!enableCache)
			Caching.CleanCache();
		StartCoroutine (DownloadAndCache());
	}
	
	IEnumerator DownloadAndCache (){
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;

//		if (CloudTargetObj.IsGame && CurrentAsset==AssetName)
//			yield return null;

		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		if(Loading)
			Loading.SetActive(true);
		using( www = WWW.LoadFromCacheOrDownload (BundleURL, version)){
			
			yield return www;
			finisheddownload = true;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);
			AssetBundle bundle = www.assetBundle;

			if(LoadedBundleObject != null) {
				this.DesroyLoadedBundleObject();
			}

			if (AssetName == ""){
				Debug.Log (AssetName);
				LoadedBundleObject = Instantiate(bundle.mainAsset);
			} else{
				AssetBundleRequest assetAsycLoader = bundle.LoadAssetAsync(AssetName);
				yield return assetAsycLoader;
				LoadedBundleObject = Instantiate(assetAsycLoader.asset as GameObject);
			//	CurrentAsset= AssetName;
				assetAsycLoader = null;
			}
			
			((GameObject)LoadedBundleObject).transform.SetParent(this.transform,true);
			// Unload the AssetBundles compressed contents to conserve memory
			bundle.Unload(false);
			if(Loading)
				Loading.SetActive(false);
			bundle = null;
		}
		// memory is freed from the web stream (www.Dispose() gets called implicitly)
		www.Dispose ();
		www = null;
	}
	
	public void DesroyLoadedBundleObject(){
		Debug.Log("DesroyLoadedBundleObject - 1");
		for (int i = 0 ; i < this.transform.childCount ; i++)
		{
			Destroy(this.transform.GetChild(i).gameObject);
		}
		Destroy(((GameObject)LoadedBundleObject));
		LoadedBundleObject = null;
	}
	
	void Update(){
		if(!finisheddownload && www != null && !www.isDone){
			if(Loading)
			{
				Debug.Log(www.progress);
				Loading.GetComponentInChildren<UnityEngine.UI.Text>().text = Mathf.Floor(www.progress * 100)+"%";
			}
			/*if(assetAsycLoader != null && assetAsycLoader.isDone ){
			}*/
		}
		
	}
	
}
