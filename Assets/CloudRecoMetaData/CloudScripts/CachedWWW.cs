﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class CachedWWW
{
    public enum CachingType { TEXTURE, TEXTASSET, VIDEO, AUDIO }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    public IEnumerator CoroutineLoadFromCacheOrDownload(string url, CachingType cachingType, Action<float> progress, Action<WWW, string> callBack)
    {
        Debug.Log("Cached WWW  URL is " + url);
        int version = 1;
        bool wwwReturned = false;
        string extension = "";
        switch (cachingType)
        {
            case CachingType.AUDIO:
                extension = "mp3";
                break;
            case CachingType.TEXTASSET:
                extension = "txt";
                break;
            case CachingType.TEXTURE:
                extension = "jpg";
                break;
            case CachingType.VIDEO:
                extension = "mp4";
                break;
        }
        string cachePath = Application.temporaryCachePath + "/" + Md5Sum(url + version) + "." + extension;
        // Only check cache if version is > 0
        if (version > 0)
        {
            // Check cache first if there is a file present
            using (WWW www = new WWW("file://" + cachePath))
            {
                while (!www.isDone)
                {
                    if (progress != null)
                        progress(www.progress);
                    yield return 0;
                }
                yield return www;
                if (www.error != null)
                {
                    Debug.Log("Didn't find a file in the cache. Proceeding to download from url.\n" + www.error);
                    // Didn't find a file in the cache.
                    // Code will proceed to download it from the url.
                }
                else
                {
                    // A file was found in the cache. Return this file, and skip the download.
                    Debug.Log(" A file was found in the cache. Return this file, and skip the download.");
                    if (progress != null)
                        progress(1);
                    if (callBack != null)
                        callBack(www, cachePath);
                    wwwReturned = true;
                }
            }
        }

        if (!wwwReturned)
        {
            using (WWW www = new WWW(url))
            {
                while (!www.isDone)
                {
                    if (progress != null)
                        progress(www.progress);
                    yield return 0;
                }
                yield return www;
                if (www.error != null)
                {
                    Debug.Log("Error downloading file :: " + www.error + " url is " + url); // if file to download is too
                    if (callBack != null)
                        callBack(null, "");
                }
                else
                {
                    // Only cache if version is 0 or higher
                    if (version >= 0)
                    {
                        switch (cachingType)
                        {
                            case CachingType.TEXTURE: // Save Texture2D to file
                                {
                                    FileStream file = File.Open(cachePath, FileMode.Create);
#if UNITY_IPHONE
							UnityEngine.iOS.Device.SetNoBackupFlag(cachePath);
#endif
                                    BinaryWriter binaryWriter = new BinaryWriter(file);
                                    binaryWriter.Write(www.bytes);
                                    file.Close();
                                }
                                break;
                            case CachingType.TEXTASSET: // Save text to file
                                {
                                    FileStream file = File.Open(cachePath, FileMode.Create);
#if UNITY_IPHONE
							UnityEngine.iOS.Device.SetNoBackupFlag(cachePath);
#endif
                                    StreamWriter streamWriter = new StreamWriter(file);
                                    streamWriter.Write(www.text);
                                    streamWriter.Close();
                                    file.Close();
                                }
                                break;
                            case CachingType.VIDEO: // Save video to file
                                {
                                    FileStream file = File.Open(cachePath, FileMode.Create);
#if UNITY_IPHONE
							UnityEngine.iOS.Device.SetNoBackupFlag(cachePath);
#endif
                                    BinaryWriter binaryWriter = new BinaryWriter(file);
                                    binaryWriter.Write(www.bytes);
                                    file.Close();
                                }
                                break;
                            case CachingType.AUDIO:
                                {
                                    FileStream file = File.Open(cachePath, FileMode.Create);
#if UNITY_IPHONE
							UnityEngine.iOS.Device.SetNoBackupFlag(cachePath);
#endif
                                    BinaryWriter binaryWriter = new BinaryWriter(file);
                                    binaryWriter.Write(www.bytes);
                                    file.Close();
                                }
                                break;
                            default:
                                throw new Exception("Trying to save unknown file type.");
                        }
                    }
                    if (progress != null)
                        progress(1);
                    if (callBack != null)
                        callBack(www, cachePath);
                }
            }
        }
    }
}
