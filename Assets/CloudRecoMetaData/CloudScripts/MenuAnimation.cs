﻿using UnityEngine;
using System.Collections;

public class MenuAnimation : MonoBehaviour {

	bool hidden = true;
	RectTransform menuRect;
	Vector3 menuPos;
	float menuWidth;

	// Use this for initialization
	void Start () {
		menuRect = GetComponent<RectTransform>();
		menuPos = menuRect.position;
		menuWidth = menuPos.x;
	}
	
	// Update is called once per frame
	void Update () {
		if(hidden) {
			if(menuPos.x > -menuWidth) {
				menuPos.x -= Time.deltaTime * 500.0f;
			}
			if(menuPos.x < -menuWidth) {
				menuPos.x = -menuWidth;
			}
		} else {
			if(menuPos.x < menuWidth) {
				menuPos.x += Time.deltaTime * 500.0f;
			}
			if(menuPos.x > menuWidth) {
				menuPos.x = menuWidth;
			}
		}
		menuRect.position = menuPos;
	}

	public void ToggleMenu() {
		hidden = !hidden;
		/*if (hidden) {
			iTween.MoveTo(gameObject,new Vector3(-183.4f,gameObject.transform.position.y,gameObject.transform.position.z),1.5f);
		} else {
			iTween.MoveTo(gameObject,new Vector3(183.4f,gameObject.transform.position.y,gameObject.transform.position.z),1.5f);
		}*/
	}
}
