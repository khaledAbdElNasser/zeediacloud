﻿using UnityEngine;
using System.Collections;

public class VideoController : MonoBehaviour {

	private MediaPlayerCtrl mPlayerCtrl;

	// Use this for initialization
	void Start () {
		mPlayerCtrl = GetComponent<MediaPlayerCtrl>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		if(mPlayerCtrl.GetCurrentState() != MediaPlayerCtrl.MEDIAPLAYER_STATE.READY) {
			Debug.Log(mPlayerCtrl.GetCurrentState());
			return;
		}

		if(mPlayerCtrl.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) {
			mPlayerCtrl.Pause();
		} else if(mPlayerCtrl.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED) {
			mPlayerCtrl.Play();
		}
	}
}
