﻿using UnityEngine;

public class calendarManager : MonoBehaviour
{

    public string website;
    CalendarData calenderData;
    public GameObject urlBtn;
    public TextAsset calenderfile;

    /*public void OpenEventUrl(){
		if (website != "" && website != null) {
			Application.OpenURL(website);
		}
	}*/


    public void initCalender(CalendarData data/*,string eventUrl*/)
    {
        calenderData = data;
        /*if (eventUrl == "" || eventUrl == null)
			urlBtn.SetActive (false);
		else {
			website = eventUrl;
			urlBtn.SetActive (true);
		}*/
    }

    public void AddEventToCalender()
    {
        Debug.Log("Openning Calender");
        Debug.Log("Calender 1");
        System.DateTime startTime;
        System.DateTime endTime;
        if (calenderData != null && calenderData.startTime != "")
            startTime = System.Convert.ToDateTime(calenderData.startTime);
        else
        {
            Debug.Log("LoadCalender - start time Date Error");
            return;
        }

        if (calenderData != null && calenderData.endTime != "")
            endTime = System.Convert.ToDateTime(calenderData.endTime);
        else
        {
            Debug.Log("LoadCalender - end time Date Error");
            return;
        }

#if UNITY_ANDROID
		// block to open the file and share it ------------START
		AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");//com.android.calendar
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","content://com.android.calendar/events");


		AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
		AndroidJavaClass calendarContractClass = new AndroidJavaClass("android.provider.CalendarContract");
		
		intentObject.Call<AndroidJavaObject> ("setAction", intentClass.GetStatic<string> ("ACTION_INSERT"));
		intentObject.Call<AndroidJavaObject> ("setData", uriObject);
		
		AndroidJavaObject calenderClass = new AndroidJavaClass ("java.util.Calendar");
		AndroidJavaObject beginTime = new AndroidJavaObject ("java.util.GregorianCalendar");
		beginTime.Call ("set",startTime.Year,startTime.Month-1,startTime.Day,startTime.Hour,startTime.Minute);
		AndroidJavaObject finishTime = new AndroidJavaObject ("java.util.GregorianCalendar");
		finishTime.Call("set",endTime.Year,endTime.Month-1,endTime.Day,endTime.Hour,endTime.Minute);

		intentObject.Call<AndroidJavaObject> ("putExtra", calendarContractClass.GetStatic<string> ("EXTRA_EVENT_BEGIN_TIME"), beginTime.Call<long>("getTimeInMillis"));
		intentObject.Call<AndroidJavaObject> ("putExtra", calendarContractClass.GetStatic<string> ("EXTRA_EVENT_END_TIME"), finishTime.Call<long>("getTimeInMillis"));
		intentObject.Call<AndroidJavaObject> ("putExtra", "title", calenderData.eventTitle);
		intentObject.Call<AndroidJavaObject> ("putExtra", "description", calenderData.eventdescription);
		intentObject.Call<AndroidJavaObject> ("putExtra", "eventLocation", calenderData.eventLocation);
		
		
		AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject> ("createChooser", intentObject, "Add Event");
		currentActivity.Call ("startActivity", jChooser);
#endif

#if UNITY_IPHONE

		/*Debug.Log("Prepare Calender  1");
		string calPath = Application.temporaryCachePath + "/" + Md5Sum(startTime.ToLongDateString() + endTime.ToLongDateString()+Time.time)+".ics";
		Debug.Log("Prepare Calender  2");
		FileStream file = File.Open(calPath, FileMode.Create);
		//UnityEngine.iOS.Device.SetNoBackupFlag(calPath);
		Debug.Log("Prepare Calender  3");
		StreamWriter streamWriter = new StreamWriter(file);
		streamWriter.Write(calenderfile.text);
		streamWriter.Close();
		file.Close();
		Debug.Log("Prepare Calender  4");
		Application.OpenURL("file://"+calPath);*/
		/*System.DateTime unixEpoch = new System.DateTime(2001,1,1,0,0,0);
		double intervalSince1970 = (startTime - unixEpoch).TotalSeconds;
		Application.OpenURL("calshow:" + intervalSince1970.ToString());*/
		//Test URL is http://www.mapsahmedf.byethost11.com/connect.php?filename=test&description=DESCRIBTION&calname=CALENDERNAME&eventtitle=EVENTTITLE&dateend=20160709T200000&datestart=20160706T070000&eventuri=TESTWEBSITE&location=LOCATION

		string iOSCalFileReq = "mapsahmedf.byethost11.com/connect.php?"+
			"filename="+Md5Sum(startTime.ToLongDateString() + endTime.ToLongDateString()+Time.time) + 
			"&description="+ WWW.EscapeURL(calenderData.eventdescription) +
			"&calname=Calendar" +
			"&eventtitle=" + WWW.EscapeURL(calenderData.eventTitle) + 
			"&dateend=" + endTime.ToString("yyyyMMdd")+ "T"+endTime.ToString("HHmmss") +
			"&datestart=" + startTime.ToString("yyyyMMdd")+ "T"+startTime.ToString("HHmmss") +
			"&eventuri=" + ((website == "" )?"None":website) +
				"&location=" + WWW.EscapeURL(calenderData.eventLocation);
		Debug.Log("Calender Url is " + iOSCalFileReq);

		Application.OpenURL("http://"+iOSCalFileReq);

		Debug.Log("Prepare Calender  5");

#endif
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}

public class CalendarData
{
    public string eventTitle;
    public string eventLocation;
    public string startTime;
    public string endTime;
    public string eventdescription;
}

